## Required software

**The following software is required:**

1. Java JDK 1.8 or above
2. Apache Maven 3.3.3 or above
3. Git

-----------------


## Working with project

**Download**

$ git clone https://oksana_stryzhak@bitbucket.org/oksana_stryzhak/semantictestproject.git

**OR**

$ git clone git@bitbucket.org:oksana_stryzhak/semantictestproject.git

## Run tests

$ mvn clean test -Dconfig.file=config.properties

## Generate report

$ mvn allure:serve

Allure report will be generated from test artefacts at target/allure-results directory and automatically opened in default browser.