package com.semantic.properties;

import com.semantic.logging.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    private static PropertiesLoader instance = null;

    private final Properties properties = new Properties();

    private PropertiesLoader() {
        try {
            loadProperties(System.getProperty(PropertyNames.CONFIG_FILE));
        } catch (final IOException e) {
            throw new IllegalStateException("Failed to load environment configuration file", e);
        }
    }

    public static String getProperty(final String propertyName) {
        if (instance == null) {
            instance = new PropertiesLoader();
        }
        return System.getProperty(propertyName, instance.properties.getProperty(propertyName));
    }

    private void loadProperties(final String resource) throws IOException {
        Logger.out.info("Reading project properties: %s", resource);
        final InputStream inputStream = getClass().getResourceAsStream(resource);
        if (inputStream == null) {
            throw new IOException("Unable to open stream for resource " + resource);
        }
        final Properties props = new Properties();
        props.load(inputStream);
        inputStream.close();
        properties.putAll(props);
    }
}
