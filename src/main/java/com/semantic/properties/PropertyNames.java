package com.semantic.properties;

public class PropertyNames {
    public static final String CONFIG_FILE = "config.file";

    public static final String BASE_SITE_URL = "base.url";
    public static final String BROWSER = "browser";

    private PropertyNames() {
    }

}
