package com.semantic.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

public class CheckboxPage {

    public final String selectedItemXPath = ".//parent::div";

    public SelenideElement standardCheckBox(){
        return $(By.xpath("//p[text()='A standard checkbox']/following-sibling::div/label"));
    }

    public SelenideElement radioBlock(){
        return $(By.xpath("//h4[text()='Radio']"));
    }

    public List<SelenideElement> inLineRadio(){
        return $$(By.xpath("//input[@name='frequency']/following-sibling::label"));
    }

    public List<SelenideElement> groupedRadio(){
        return $$(By.xpath("//input[@name='example2']/following-sibling::label"));
    }

    public SelenideElement sliderBlock(){
        return $(By.xpath("//h4[text()='Slider']"));
    }

    public SelenideElement newsLetterSlider(){
        return $(By.xpath("//input[@name='newsletter']/following-sibling::label"));
    }

    public List<SelenideElement> throughputListSlider(){
        return $$(By.xpath("//input[@name='throughput']/following-sibling::label"));
    }

    public SelenideElement toggleBlock() {
        return $(By.xpath("//h4[text()='Toggle']"));
    }

    public SelenideElement publicToggle() {
        return $(By.xpath("//input[@name='public']/following-sibling::label"));
    }

    @Step()
    public void openPage() {
        open("/modules/checkbox.html");
    }
}

