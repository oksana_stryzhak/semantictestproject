package com.semantic.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class TablePage {

    public String attentionIconXPath = ".//*[@class='attention icon']";

    public SelenideElement positiveNegativeTable() {
        return $(By.xpath("//h4[text()='Positive / Negative']/parent::div"));
    }

    public SelenideElement errorTable() {
        return $(By.xpath("//h4[text()='Error']/parent::div"));
    }

    public SelenideElement warningTable() {
        return $(By.xpath("//h4[text()='Warning']/parent::div"));
    }

    @Step()
    public void openPage() {
        open("/collections/table.html");
    }
}