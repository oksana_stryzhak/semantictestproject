package com.semantic.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class DropDownPage {

    public SelenideElement selectionBlock(){
        return $(By.xpath("//h4[text()='Selection']/parent::div"));
    }

    public SelenideElement firstGenderDropDown(){
        return selectionBlock().find(By.xpath("./div[contains(@class,'ui selection dropdown')]"));
    }

    @Step
    public void selectFirstGender(final String gender){
        firstGenderDropDown().click();
        firstGenderDropDown().find(By.xpath(".//div[text()='" + gender + "']")).click();
    }

    public SelenideElement secondGenderDropDown(){
        return selectionBlock().find(By.xpath("./following::div[2]"));
    }

    @Step
    public void selectSecondGender(final String gender){
        secondGenderDropDown().click();
        secondGenderDropDown().find(By.xpath(".//div[text()='" + gender + "']")).click();
    }

    public SelenideElement friendsDropDown(){
        return selectionBlock().find(By.xpath("./following::div[8]"));
    }

    @Step
    public void selectFriend(final String friendName){
        friendsDropDown().click();
        friendsDropDown().find(By.xpath(".//div[@data-value='" + friendName.toLowerCase() + "']")).click();
    }

    @Step()
    public void openPage() {
        open("/modules/dropdown.html");
    }
}
