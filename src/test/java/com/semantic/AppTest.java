package com.semantic;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.junit.TextReport;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.semantic.logging.Logger;
import com.semantic.pages.CheckboxPage;
import com.semantic.pages.DropDownPage;
import com.semantic.pages.TablePage;
import com.semantic.properties.PropertiesLoader;
import com.semantic.properties.PropertyNames;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;

public class AppTest{

    @Rule
    public TestRule report = new TextReport();

    @Before
    public void setUp() {
        Logger.out.info("Preparing launch tests...\n===============================================");
        Configuration.browser = PropertiesLoader.getProperty(PropertyNames.BROWSER);
        Configuration.startMaximized = true;
        Configuration.baseUrl = PropertiesLoader.getProperty(PropertyNames.BASE_SITE_URL);

        // init allure report listener
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(true));
        Logger.out.info("Start running tests...\n===============================================");
    }

    @Test
    public void test1() {
        // given
        final TablePage tablePage = new TablePage();

        // when
        tablePage.openPage();

        // then
        final SelenideElement errorTable = tablePage.errorTable();
        errorTable.scrollTo()
                .find(By.xpath(".//td[text()='Jimmy']/following-sibling::td[1]"))
                .shouldHave(text("Cannot pull data"));
        // check status and notes for 'No Name Specified'
        errorTable.find(By.xpath(".//td[text()='No Name Specified']/following-sibling::td[1]"))
                .shouldHave(text("Approved"));
        errorTable.find(By.xpath(".//td[text()='No Name Specified']//following-sibling::td[2]"))
                .shouldHave(text("None"));
        // check status and notes for 'Jill'
        errorTable.find(By.xpath(".//td[text()='Jill']/following-sibling::td[1]"))
                .shouldHave(text("Approved"));
        errorTable.find(By.xpath(".//td[text()='Jill']/following-sibling::td[2]"))
                .shouldHave(text("None"));

        // check attention icon at warningTable
        final SelenideElement warningTable = tablePage.warningTable();
        // for Jimmy row
        warningTable.scrollTo().find(By.xpath(".//td[text()='Jimmy']/ancestor::tr"))
                .find(By.xpath(tablePage.attentionIconXPath)).shouldBe(visible);
        // for Jamie row
        warningTable.find(By.xpath(".//td[text()='Jamie']/ancestor::tr"))
                .find(By.xpath(tablePage.attentionIconXPath)).should(visible);
    }
    @Test
    public void test2() {
        // given
        final String male = "Male";
        final String female = "Female";
        final String name = "Christian";
        final DropDownPage dropDownPage = new DropDownPage();

        // when
        dropDownPage.openPage();
        dropDownPage.selectionBlock().scrollTo();
        dropDownPage.selectFirstGender(male);
        dropDownPage.selectSecondGender(female);
        dropDownPage.selectFriend(name);

        // then
        // check dropDown selected items
        dropDownPage.firstGenderDropDown().shouldHave(text(male));
        dropDownPage.secondGenderDropDown().shouldHave(text(female));
        dropDownPage.friendsDropDown().shouldHave(text(name));
    }

    @Test
    public void test3() {
        // given
        CheckboxPage checkboxPage = new CheckboxPage();

        // when
        checkboxPage.openPage();

        checkboxPage.standardCheckBox().click();

        checkboxPage.radioBlock().scrollTo();
        // select items at radioBlocks
        checkboxPage.inLineRadio().get(3).click();
        checkboxPage.groupedRadio().get(2).click();

        checkboxPage.sliderBlock().scrollTo();
        // select items at sliderBlock
        checkboxPage.newsLetterSlider().click();
        checkboxPage.throughputListSlider().get(3).click();

        // select item at toggleBlock
        checkboxPage.toggleBlock().scrollTo();
        checkboxPage.publicToggle().click();

        // then
        // check css class value for checkBox, radioBlock, sliderBlock, toggleBlock
        checkboxPage.standardCheckBox().find(By.xpath(checkboxPage.selectedItemXPath)).shouldHave(Condition.cssClass("checked"));
        checkboxPage.inLineRadio().get(3).find(By.xpath(checkboxPage.selectedItemXPath)).shouldHave(Condition.cssClass("checked"));
        checkboxPage.groupedRadio().get(2).find(By.xpath(checkboxPage.selectedItemXPath)).shouldHave(Condition.cssClass("checked"));
        checkboxPage.newsLetterSlider().find(By.xpath(checkboxPage.selectedItemXPath)).shouldHave(Condition.cssClass("checked"));
        checkboxPage.throughputListSlider().get(3).find(By.xpath(checkboxPage.selectedItemXPath)).shouldHave(Condition.cssClass("checked"));
        checkboxPage.publicToggle().find(By.xpath(checkboxPage.selectedItemXPath)).shouldHave(Condition.cssClass("checked"));
    }
}
